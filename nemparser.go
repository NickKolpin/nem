// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package nem

import (
	"fmt"
	"strings"
)

func NewNEM() *NEMParser {
	paper := "a4paper"
	documentclass := "article"
	return &NEMParser{
		Paper:         paper,
		DocumentClass: documentclass}
}

type NEMParser struct {
	Paper         string
	DocumentClass string
}

func (n *NEMParser) Tag() string {
	return "nem"
}

func (n *NEMParser) Verbatim() bool {
	return false
}

func (n *NEMParser) Inline(id ID, in string) (Warning, error) {
	return NewWarning(""), fmt.Errorf("NEM can only be meta, not inline")
}

func (n *NEMParser) Block(id ID, in string) (Warning, error) {
	return NewWarning(""), fmt.Errorf("NEM can only be meta, not inline")
}

func (n *NEMParser) Meta(in string) (Warning, error) {
	in = strings.Replace(in, "\n", "", -1)
	args := strings.Split(in, "|")
	for _, arg := range args {
		if arg == "" {
			continue
		}
		parts := strings.Split(arg, "=")
		if len(parts) > 2 {
			parts[1] = strings.Join(parts[1:], "=")
		}
		a := parts[0]
		v := ""
		if len(parts) > 1 {
			v = parts[1]
		}
		switch a {
		case "paper":
			n.Paper = v
		case "documentclass":
			n.DocumentClass = v
		default:
			return NewWarning(fmt.Sprintf("NEMParser: unknown arg %s", a)), nil
		}
	}
	return nil, nil
}

func (n *NEMParser) LaTex(id ID) string {
	return ""
}

func (n *NEMParser) LaTexPreamble() string {
	return fmt.Sprintf("\\documentclass[%s]{%s}", n.Paper, n.DocumentClass)
	return ""
}

func (n *NEMParser) HTML(id ID) string {
	return ""
}

func (n *NEMParser) HTMLHeader() string {
	return ""
}

func (n *NEMParser) Labels() map[string]string {
	// Shouldn't be any labels for the title
	return make(map[string]string)
}
