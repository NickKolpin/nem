Nick's Extendable Markup
========================

NEM is a markup language specification and parser that allows text based documents to be rendered into PDF or HTML pages.
The language is designed to be extendable with the addtion of new block types.

Licenses
--------

The software for NEM (Copyright 2017, Nick Kolpin) is made available under an open source three clause BSD Licese.
See LICENSE.txt for details.

The specification is availble under a CC-BY-ND license, see https://creativecommons.org/licenses/by-nd/4.0/


Example documents
-----------------

The examples/ directory will be populated with example documents including the NEM source document and the built PDF and HTML files.

Specification
-------------

See spec/NEM_spec_CC-BY-ND.md for the markup specification.
