// Copyright 2009 The Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Nick's extendable markup program
package main

import (
	"bitbucket.org/NickKolpin/nem"
	"bitbucket.org/NickKolpin/nem/std"
	"flag"
	"fmt"
	"log"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Llongfile)
	doc := nem.New()
	// Add standard objects
	if err := doc.Add(std.NewText()); err != nil {
		log.Fatal(err)
	}
	if err := doc.Add(std.NewTitle()); err != nil {
		log.Fatal(err)
	}
	if err := doc.Add(std.NewHTML()); err != nil {
		log.Fatal(err)
	}
	if err := doc.Add(std.NewLaTex()); err != nil {
		log.Fatal(err)
	}
	if err := doc.Add(std.NewSection()); err != nil {
		log.Fatal(err)
	}
	fmt.Printf("document: %+v\n", doc)
	// Add third party objects here
	/* eg
	   if err := doc.Add(foo.New(x, y, z)); err != nil {
	       log.Fatal(err)
	   }
	*/
	writepdf := flag.Bool("pdf", false, "Output PDF document compiled using LaTex")
	writetex := flag.Bool("tex", false, "Output tex document")
	writehtml := flag.Bool("html", false, "Output HTML document")
	pedantic := flag.Bool("pedantic", false, "Elevate parser warnings to errors")
	flag.Parse()
	args := flag.Args()
	if len(args) != 1 {
		log.Fatalf("Must provide single input document")
	}
	errors, warnings := doc.Parse(args[0])
	for _, err := range errors {
		log.Printf("Error: %v\n", err)

	}
	if len(errors) > 0 {
		log.Fatalf("%d parser errors", len(errors))
	}
	for _, warn := range warnings {
		log.Printf("Warning: %v\n", warn)
	}
	if *pedantic && len(warnings) > 0 {
		log.Fatalf("%d parser warnings, pedantic mode", len(warnings))
	}
	if *writepdf || *writetex {
		if err := doc.LaTex(*writepdf); err != nil {
			log.Fatal(err)
		}
	}
	if *writehtml {
		if err := doc.HTML(); err != nil {
			log.Fatal(err)
		}
	}
}
