// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package nem

import (
	"fmt"
	"strings"
)

func NewRef() *RefParser {
	refs := make(map[ID]Ref)
	labs := make(map[string]string)
	return &RefParser{refs: refs, labels: labs}
}

type RefParser struct {
	refs   map[ID]Ref
	labels map[string]string
}

func (r *RefParser) Tag() string {
	return "ref"
}

func (r *RefParser) Verbatim() bool {
	return true
}

func (r *RefParser) Inline(id ID, in string) (Warning, error) {
	f := string(in[0])
	fu := strings.ToUpper(f)
	upper := f == fu
	ref := Ref{ID: id, Upper: upper, Ref: in, Label: ""}
	r.refs[id] = ref
	return nil, nil
}

func (r *RefParser) Block(id ID, in string) (Warning, error) {
	return NewWarning(""), fmt.Errorf("Ref can only be inline, not block")
}

func (r *RefParser) Meta(in string) (Warning, error) {
	return NewWarning(""), fmt.Errorf("Ref can only be inline, not meta")
}

func (r *RefParser) LaTex(id ID) string {
	ref, ok := r.refs[id]
	if !ok {
		return ""
	}
	return ref.LaTex()
}

func (r *RefParser) LaTexPreamble() string {
	return "\\usepackage[noabbrev]{cleveref}"
}

func (r *RefParser) HTML(id ID) string {
	ref, ok := r.refs[id]
	if !ok {
		return ""
	}
	return ref.HTML()
}

func (r *RefParser) HTMLHeader() string {
	return ""
}

func (r *RefParser) ResolveLabels(labels map[string]string) {
	r.labels = labels
	for id, ref := range r.refs {
		lab, ok := r.labels[ref.Ref]
		if ok {
			ref.Label = lab
			r.refs[id] = ref
		}
	}
}

func (r *RefParser) Labels() map[string]string {
	return make(map[string]string)
}

type Ref struct {
	ID    ID
	Upper bool
	Ref   string
	Label string
}

func (r Ref) LaTex() string {
	if r.Upper {
		return fmt.Sprintf("\\Cref{%s}", r.Ref)
	}
	return fmt.Sprintf("\\cref{%s}", r.Ref)
}

func (r Ref) HTML() string {
	label := r.Label
	if r.Upper {
		ch := string(label[0])
		strings.Replace(label, ch, strings.ToUpper(ch), 1)
	}
	return fmt.Sprintf("<a href=\"#%s\">%s</a>", r.Ref, label)
}
