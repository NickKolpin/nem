// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package std

import (
	"bitbucket.org/NickKolpin/nem"
	"fmt"
	"sort"
	"strings"
)

func NewSection() *SectionParser {
	secs := make(map[nem.ID]Section)
	return &SectionParser{Sections: secs}
}

type SectionParser struct {
	ToCID    nem.ID
	ToC      bool
	Sections map[nem.ID]Section
}

func (s *SectionParser) Tag() string {
	return "section"
}

func (s *SectionParser) Verbatim() bool {
	return false
}

func (s *SectionParser) Inline(id nem.ID, in string) (nem.Warning, error) {
	return nil, fmt.Errorf("Section can only be block, not inline")
}

func (s *SectionParser) Block(id nem.ID, in string) (nem.Warning, error) {
	in = strings.Replace(in, "\n", "", -1)
	if in == "ToC" {
		s.ToC = true
		s.ToCID = id
		return nil, nil
	}
	args := strings.Split(in, "|")
	sec := Section{}
	for _, arg := range args {
		if arg == "" {
			continue
		}
		if strings.Contains(arg, "=") {
			parts := strings.Split(arg, "=")
			if parts[0] == "label" {
				sec.Label = parts[1]
			}
		} else if arg == "sub" {
			sec.Level = 1
		} else if arg == "subsub" {
			sec.Level = 2
		} else {
			sec.Text = arg
		}
	}
	s.Sections[id] = sec
	return nil, nil
}

func (s *SectionParser) Meta(in string) (nem.Warning, error) {
	return nil, fmt.Errorf("Section can only be block, not meta")
}

func (s *SectionParser) LaTex(id nem.ID) string {
	if s.ToC && id == s.ToCID {
		return "\\tableofcontents"
	}
	sec, ok := s.Sections[id]
	if !ok {
		return ""
	}
	l := ""
	switch sec.Level {
	case 0:
		l += "\\section{"
	case 1:
		l += "\\subsection{"
	case 2:
		l += "\\subsubsection{"
	}
	l += sec.Text + "}"
	if sec.Label != "" {
		l += fmt.Sprintf("\n\\label{%s}", sec.Label)
	}
	return l
}

func (s *SectionParser) LaTexPreamble() string {
	return "\\usepackage{hyperref}"
}

func (s *SectionParser) HTML(id nem.ID) string {
	if s.ToC && id == s.ToCID {
		return s.HTMLTableOfContents()
	}
	sec, ok := s.Sections[id]
	if !ok {
		return ""
	}
	h := ""
	lev := strings.Replace(sec.Tag, "section ", "", 1)
	switch sec.Level {
	case 0:
		if sec.Label != "" {
			h += fmt.Sprintf("<h2 id=\"%s\">%s  %s</h2>", sec.Label, lev, sec.Text)
		} else {
			h += fmt.Sprintf("<h2>%s</h2>", sec.Text)
		}
	case 1:
		if sec.Label != "" {
			h += fmt.Sprintf("<h3 id=\"%s\">%s  %s</h3>", sec.Label, lev, sec.Text)
		} else {
			h += fmt.Sprintf("<h3>%s</h3>", sec.Text)
		}
	case 2:
		if sec.Label != "" {
			h += fmt.Sprintf("<h4 id=\"%s\">%s  %s</h4>", sec.Label, lev, sec.Text)
		} else {
			h += fmt.Sprintf("<h4>%s</h4>", sec.Text)
		}
	}
	return h
}

func (s *SectionParser) HTMLHeader() string {
	return ""
}

func (s *SectionParser) HTMLTableOfContents() string {
	h := "<h2>Contents</h2>\n"
	h += "<ul>\n"
	ids := make([]nem.ID, 0, len(s.Sections))
	for id, _ := range s.Sections {
		ids = append(ids, id)
	}
	sort.Slice(ids, func(i, j int) bool { return uint32(ids[i]) < uint32(ids[j]) })
	for _, id := range ids {
		sec := s.Sections[id]
		lev := strings.Replace(sec.Tag, "section ", "", 1)
		h += fmt.Sprintf("<li><a href=\"#%s\">%s %s</a></li>\n", sec.Label, lev, sec.Text)
	}
	h += "</ul>"
	return h
}

func (s *SectionParser) Labels() map[string]string {
	ids := make([]nem.ID, 0, len(s.Sections))
	for id, _ := range s.Sections {
		ids = append(ids, id)
	}
	sort.Slice(ids, func(i, j int) bool { return uint32(ids[i]) < uint32(ids[j]) })
	labels := make(map[string]string)
	l0 := 0
	l1 := 0
	l2 := 0
	for _, id := range ids {
		sec := s.Sections[id]
		switch sec.Level {
		case 0:
			l0++
			l1 = 0
			l2 = 0
		case 1:
			l1++
			l2 = 0
		case 2:
			l2++
		}
		lev := fmt.Sprintf("%d", l0)
		if l1 != 0 || l2 != 0 {
			lev += fmt.Sprintf(".%d", l1)
		}
		if l2 != 0 {
			lev += fmt.Sprintf(".%d", l2)
		}
		sec.Tag = fmt.Sprintf("section %s", lev)
		if sec.Label != "" {
			labels[sec.Label] = sec.Tag
		} else {
			sec.Label = fmt.Sprintf("sec:%s", lev)
		}
		s.Sections[id] = sec
	}
	return labels
}

type Section struct {
	Text  string
	Label string
	Tag   string
	Level int
}
