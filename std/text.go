// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package std

import (
	"bitbucket.org/NickKolpin/nem"
	"fmt"
)

func NewText() *TextParser {
	texts := make(map[nem.ID]string)
	return &TextParser{Texts: texts}
}

type TextParser struct {
	Texts map[nem.ID]string
}

func (tp *TextParser) Tag() string {
	return "text"
}

func (tp *TextParser) Verbatim() bool {
	return false
}

func (tp *TextParser) Inline(id nem.ID, in string) (nem.Warning, error) {
	return nem.NewWarning(""), fmt.Errorf("Text can only be block, not inline")
}

func (tp *TextParser) Block(id nem.ID, in string) (nem.Warning, error) {
	tp.Texts[id] = in
	return nil, nil
}

func (tp *TextParser) Meta(in string) (nem.Warning, error) {
	return nem.NewWarning(""), fmt.Errorf("Text can only be block, not meta")
}

func (tp *TextParser) LaTex(id nem.ID) string {
	// Only a single text exists, so n is ignored
	s, ok := tp.Texts[id]
	if !ok {
		return ""
	}
	return s
}

func (tp *TextParser) LaTexPreamble() string {
	return ""
}

func (tp *TextParser) HTML(id nem.ID) string {
	// Only a single text exists, so n is ignored
	s, ok := tp.Texts[id]
	if !ok {
		return ""
	}
	return fmt.Sprintf("<p>\n%s\n</p>", s)
}

func (tp *TextParser) HTMLHeader() string {
	return ""
}

func (tp *TextParser) Labels() map[string]string {
	// Shouldn't be any labels for the text
	return make(map[string]string)
}
