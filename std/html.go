// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package std

import (
	"bitbucket.org/NickKolpin/nem"
	"fmt"
	"strings"
)

func NewHTML() *HTMLParser {
	b := make(map[nem.ID]HTML)
	return &HTMLParser{blocks: b}
}

type HTMLParser struct {
	blocks map[nem.ID]HTML
}

func (h *HTMLParser) Tag() string {
	return "html"
}

func (h *HTMLParser) Verbatim() bool {
	return true
}

func (h *HTMLParser) Inline(id nem.ID, in string) (nem.Warning, error) {
	block := HTML{data: in}
	h.blocks[id] = block
	return nil, nil
}

func (h *HTMLParser) Block(id nem.ID, in string) (nem.Warning, error) {
	block := HTML{data: strings.Join(strings.Split(in, "\n")[1:], "\n")}
	h.blocks[id] = block
	return nil, nil
}

func (h *HTMLParser) Meta(in string) (nem.Warning, error) {
	return nem.NewWarning(""), fmt.Errorf("Not implemented")
}

// HTMLParser only outputs for a HTML document
func (h *HTMLParser) LaTex(id nem.ID) string {
	return ""
}

func (h *HTMLParser) LaTexPreamble() string {
	return ""
}

func (h *HTMLParser) HTML(id nem.ID) string {
	b, ok := h.blocks[id]
	if !ok {
		return ""
	}
	return b.data
	return ""
}

func (h *HTMLParser) HTMLHeader() string {
	return "<script type=\"text/javascript\" src=\"https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML\"></script>"
}

func (h *HTMLParser) Labels() map[string]string {
	return make(map[string]string)
}

type HTML struct {
	data string
}
