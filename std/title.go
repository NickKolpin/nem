// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package std

import (
	"bitbucket.org/NickKolpin/nem"
	"fmt"
	"strings"
	"time"
)

func NewTitle() *TitleParser {
	return &TitleParser{}
}

type TitleParser struct {
	title Title
}

func (tp *TitleParser) Tag() string {
	return "title"
}

func (tp *TitleParser) Verbatim() bool {
	return false
}

func (tp *TitleParser) Inline(id nem.ID, in string) (nem.Warning, error) {
	return nem.NewWarning(""), fmt.Errorf("Title can only be block, not inline")
}

func (tp *TitleParser) Block(id nem.ID, in string) (nem.Warning, error) {
	return tp.Meta(in)
}

func (tp *TitleParser) Meta(in string) (nem.Warning, error) {
	in = strings.Replace(in, "\n", "", -1)
	args := strings.Split(in, "|")
	for _, arg := range args {
		if arg == "" {
			continue
		}
		parts := strings.Split(arg, "=")
		if len(parts) > 2 {
			parts[1] = strings.Join(parts[1:], "=")
		}
		a := parts[0]
		v := parts[1]
		switch a {
		case "title":
			tp.title.title = v
		case "date":
			tp.title.date = v
		case "author":
			tp.title.authors = append(tp.title.authors, v)
		default:
			return nem.NewWarning(fmt.Sprintf("TitleParser: unknown arg %s", a)), nil
		}
	}
	return nil, nil
}

func (tp *TitleParser) LaTex(id nem.ID) string {
	// Only a single title exists, so n is ignored
	return tp.title.LaTex()
}

func (tp *TitleParser) LaTexPreamble() string {
	return ""
}

func (tp *TitleParser) HTML(id nem.ID) string {
	// Only a single title exists, so n is ignored
	return tp.title.HTML()
}

func (tp *TitleParser) HTMLHeader() string {
	return fmt.Sprintf("<title>%s</title>", tp.title.title)
}

func (tp *TitleParser) Labels() map[string]string {
	// Shouldn't be any labels for the title
	return make(map[string]string)
}

type Title struct {
	title        string
	authors      []string
	affiliations []string
	date         string
}

func (t Title) LaTex() string {
	// No substitutions are allowed/expected in the title
	s := ""
	s += fmt.Sprintf("\\title{%s}\n", t.title)
	// Currently just hard code a single author, no affiliations
	auth := "Nobody"
	if len(t.authors) > 0 {
		auth = t.authors[0]
	}
	s += fmt.Sprintf("\\author{%s}\n", auth)
	if t.date == "" {
		t.date = "\\today"
	}
	s += fmt.Sprintf("\\date{%s}\n", t.date)
	s += "\\maketitle{}\n"
	return s
}

func (t Title) HTML() string {
	auth := ""
	if len(t.authors) > 0 {
		auth = t.authors[0] + ", "
	}
	date := time.Now().Format("02 Jan 06")
	if t.date != "" {
		date = t.date
	}
	return fmt.Sprintf("<h1>%s</h1>\n<i>%s%s</i>", t.title, auth, date)
}
