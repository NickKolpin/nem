// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package std

import (
	"bitbucket.org/NickKolpin/nem"
	"fmt"
	"strings"
)

func NewLaTex() *LaTexParser {
	b := make(map[nem.ID]LaTex)
	return &LaTexParser{blocks: b}
}

type LaTexParser struct {
	blocks map[nem.ID]LaTex
}

func (h *LaTexParser) Tag() string {
	return "latex"
}

func (h *LaTexParser) Verbatim() bool {
	return true
}

func (h *LaTexParser) Inline(id nem.ID, in string) (nem.Warning, error) {
	block := LaTex{data: in}
	h.blocks[id] = block
	return nil, nil
}

func (h *LaTexParser) Block(id nem.ID, in string) (nem.Warning, error) {
	l := LaTex{data: strings.Join(strings.Split(in, "\n")[1:], "\n")}
	h.blocks[id] = l
	return nil, nil
}

func (h *LaTexParser) Meta(in string) (nem.Warning, error) {
	return nem.NewWarning(""), fmt.Errorf("Not implemented")
}

// LaTexParser only outputs for a LaTex document
func (h *LaTexParser) HTML(id nem.ID) string {
	return ""
}

func (h *LaTexParser) HTMLHeader() string {
	return ""
}

func (h *LaTexParser) LaTex(id nem.ID) string {
	b, ok := h.blocks[id]
	if !ok {
		return ""
	}
	return b.data
}

func (h *LaTexParser) LaTexPreamble() string {
	return ""
}

func (h *LaTexParser) Labels() map[string]string {
	return make(map[string]string)
}

type LaTex struct {
	data string
}
