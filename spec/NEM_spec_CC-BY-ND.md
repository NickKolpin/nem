Specification v0.1
==================

_This is a draft specification for Nick's Extendable Markup language.
Copyright 2017 Nicholas Kolpin.
This document is released under a Creative Commons Attribution-NoDerivatives license.
You may share this unmodified copies of this document for commercial and non-commercial purposes as long as you attribute the work to the author.
See https://creativecommons.org/licenses/by-nd/4.0/ for details._

[TOC]

Introduction
------------

NEM is an extendable markup language allowing a PDF or HTML document to be built from one or more text files.
The document is built from a mixture or normal text with NEM _Objects_, which can be encoded in the document using the format described in this specification.

Text
----

The simplest documents can be created using only text.
Use of text has limited rules:

* Text spanning multiple consecutive lines of the NEM document are considered as a single block, resulting in a paragraph.
* Two consecutive newlines separating text (as well as the beginning or end of the document) delimit these blocks.

Comments
--------

NEM uses C-style comments.
Two consecituve slashes `//` cause the remainder of the line to be a comment.
Block comments are enclosed between `/*` and `*/`.

NEM Objects
-----------

NEM _Objects_ represent addtional formatting of the text, such as sections, figures, links, etc.
NEM Objects are represented in the text enclosed in brackets.
There are three types of brackets used:

* Square brackets `[` and `]` delimit an in-line object, intended to insert an object within the flow of the current text block.
* Angled brackets `<` and `>` delimit an NEM object which stands alone as a block. The opening bracket should be follow a two newlines and the closing bracket should be followed by two newlines. Within such an object two consecutive new lines do not necessarily start a new block.
* Curly brackets `{` and `}` delimit a _meta-Object_, which is not intended to be (directly) included in the output document. These can be used to set parameters or to define an _Object_ whose inclusion in the output document will be via another Object.
* Each Object is delimited by an opening and closing bracket of the same type.

Each Object has a type specifier, which immediately follows the opening bracket.
The type specifier should be a short string, either in lower case or camelCase.
The type specifier must be followed directly by a space.
For example:

````
// Meta-Object for git repository referred to by other Objects
{git repo|URL=bitbucket.org/NickKolpin/nem|name=NEM}

// Insert details from a git repo's issue tracker
<git issue|NEM|3>

// Insert current date, this Object needs no arguments.
Today is [date].
````

If required the type specifier can be followed by a list of pipe `|` spearated arguments, which will be interpreted by the Object's parser.
The format of these arguments is not defined by the specification, but generally either single word variables or `key=value` pairs.

Objects can be nested within each other, however they cannot overlap.
If the opening tag for an object is nested within an outer object, its closing tag must also be within the same outer object.
There is no limit to the level of nesting possible.

Output documents
----------------

NEM supports building HTML and/or PDF output documents.
PDFs are built using pdflatex.

Extendability
-------------

NEM is designed to be easily extended.
This can be done by writing new software which parses a new Object type and builds snippets for the supported output document types.


