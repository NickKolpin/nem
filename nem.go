// Copyright 2017 Nicholas Kolpin. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package nem

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"
)

type Warning interface {
	Warning() string
}

type Warn struct {
	w string
}

func (w Warn) Warning() string {
	return w.w
}

func NewWarning(w string) Warning {
	return Warn{w}
}

type ID uint32

type Object interface {
	Tag() string
	Inline(ID, string) (Warning, error)
	Block(ID, string) (Warning, error)
	Meta(string) (Warning, error)
	LaTex(ID) string
	LaTexPreamble() string
	HTML(ID) string
	HTMLHeader() string
	Labels() map[string]string
	Verbatim() bool
}

type Block struct {
	tag      string
	ID       ID
	Children []Block
}

func New() *Document {
	objs := make(map[string]Object)
	all := make(map[ID]Object)
	str := make([]Block, 0, 128)
	ordered := []Object{}
	doc := &Document{Objects: objs, Structure: str, all: all, ordered: ordered}
	doc.Add(NewNEM())
	return doc
}

type Document struct {
	Objects   map[string]Object
	all       map[ID]Object
	ordered   []Object
	Structure []Block
	fn        string
	nextid    ID
}

func (d *Document) ID() ID {
	id := d.nextid
	d.nextid++
	return id
}

func (d *Document) Add(obj Object) error {
	tag := obj.Tag()
	if _, ok := d.Objects[tag]; ok {
		return fmt.Errorf("Tag clash: %s", tag)
	}
	d.Objects[tag] = obj
	d.ordered = append(d.ordered, obj)
	return nil
}

func (d *Document) Parse(fn string) ([]error, []Warning) {
	d.Add(NewRef())
	d.fn = fn
	// Split document into blocks
	bdata, err := ioutil.ReadFile(fn)
	if err != nil {
		return []error{err}, []Warning{}
	}
	blocks := []string{}
	data := strings.Split(string(bdata), "\n\n")
	// Check for block types and join object/meta blocks spanning multiple blocks
	iblk := 0
	nblk := len(data)
	for iblk < nblk {
		blk := data[iblk]
		switch blk[0] {
		case '<':
			b := ""
			inblk := true
			for inblk && iblk < nblk {
				blk = data[iblk]
				b += blk + "\n\n"
				lines := strings.Split(blk, "\n")
				if lines[len(lines)-1] == ">" {
					inblk = false
				} else {
					iblk++
				}
			}
			blocks = append(blocks, b)
		case '{':
			b := ""
			inblk := true
			for inblk {
				blk = data[iblk]
				b += blk + "\n\n"
				lines := strings.Split(blk, "\n")
				if lines[len(lines)-1] == "}" {
					inblk = false
				} else {
					iblk++
				}
			}
			blocks = append(blocks, b)
		default:
			blocks = append(blocks, blk)
		}
		iblk++
	}
	fmt.Printf("%d blocks found:\n", len(blocks))
	for _, blk := range blocks {
		fmt.Printf(">\t%s\n", strings.Replace(blk, "\n", "\\n", -1))
	}
	// Parse objects in each block.
	errs := []error{}
	for _, blk := range blocks {
		switch blk[0] {
		case '{', '<':
			if !strings.Contains(blk, " ") {
				errs = append(errs, fmt.Errorf("Invalid block, no space: %s", blk))
				continue
			}
			// tag is after opening bracket and before first space
			tag := strings.Split(blk, " ")[0][1:]
			obj, ok := d.Objects[tag]
			if !ok {
				errs = append(errs, fmt.Errorf("Unknown object: %s", tag))
				continue
			}
			children := []Block{}
			if !obj.Verbatim() {
				// Check for inline blocks
				childerrs := []error{}
				blk, children, childerrs = d.parseinline(blk)
				for _, err := range childerrs {
					errs = append(errs, err)
				}
			}
			s := strings.Join(strings.Split(blk, " ")[1:], " ")
			s = s[:len(s)-3]
			fmt.Printf("s = ~%s~\n", s)
			err = error(nil)
			switch blk[0] {
			case '{':
				_, err = obj.Meta(s)
			case '<':
				id := d.ID()
				_, err = obj.Block(id, s)
				d.Structure = append(d.Structure, Block{tag: tag, ID: id, Children: children})
				d.all[id] = obj
				id++
			}
			if err != nil {
				errs = append(errs, err)
			}
		default:
			tag := "text"
			id := d.ID()
			obj, ok := d.Objects[tag]
			if !ok {
				errs = append(errs, fmt.Errorf("Unknown object: %s", tag))
				continue
			}
			children := []Block{}
			if !obj.Verbatim() {
				fmt.Printf("Checking text block %d for in-line blocks\n", id)
				// Check for inline blocks
				childerrs := []error{}
				blk, children, childerrs = d.parseinline(blk)
				for _, err := range childerrs {
					errs = append(errs, err)
				}
			}
			err = error(nil)
			_, err = obj.Block(id, blk)
			d.Structure = append(d.Structure, Block{tag: tag, ID: id, Children: children})
			d.all[id] = obj
			id++
			id += ID(len(children))
			if err != nil {
				errs = append(errs, err)
			}
		}
	}
	fmt.Printf("Document structure:\n%+v\n", d.Structure)
	labels := make(map[string]string)
	r := d.Objects["ref"]
	refs := r.(*RefParser)
	for _, obj := range d.Objects {
		objlabels := obj.Labels()
		for ref, label := range objlabels {
			labels[ref] = strings.ToLower(label)
		}
	}
	fmt.Printf("Labels: %+v\n", labels)
	refs.ResolveLabels(labels)
	fmt.Printf("References: %+v\n", refs)
	fmt.Printf("all: %+v\n", d.all)
	fmt.Printf("errors: %+v\n", errs)
	return errs, []Warning{}
}

func (d *Document) parseinline(blk string) (string, []Block, []error) {
	children := []Block{}
	errs := []error{}
	lines := strings.Split(blk, "\n")
	for _, line := range lines {
		if strings.Contains(line, "[") {
			fmt.Printf("inline in: '%s'?\n", line)
			parts := strings.Split(line, "[")
			// Currently only non nested tags, but potentially multiple of them
			for _, p := range parts[1:] {
				fmt.Printf("p = '%s'\n", p)
				if !strings.Contains(p, "]") {
					fmt.Printf("Closing square bracket not found.\n")
					break
				}
				allinline := strings.Split(p, "]")[0]
				inline := allinline
				tag := strings.Split(inline, " ")[0]
				if strings.Contains(inline, " ") {
					inline = strings.Join(strings.Split(inline, " ")[1:], " ")
				} else {
					inline = ""
				}
				fmt.Printf("inline = '%s'\n", inline)
				subobj, ok := d.Objects[tag]
				if !ok {
					errs = append(errs, fmt.Errorf("Unknown object: %s", tag))
					continue
				}
				id := d.ID()
				_, err := subobj.Inline(id, inline)
				if err != nil {
					errs = append(errs, err)
					continue
				}
				d.all[id] = subobj
				child := Block{tag: tag, ID: id, Children: []Block{}}
				children = append(children, child)
				blk = strings.Replace(blk, allinline, fmt.Sprintf("NEM:%d", id), 1)
			}
		}
	}
	return blk, children, errs
}

func (d *Document) LaTex(pdf bool) error {
	if d.fn == "" {
		return fmt.Errorf("Cannot build LaTex document: input not parsed")
	}
	out := d.Objects["nem"].LaTexPreamble() + "\n"
	for _, obj := range d.ordered {
		if obj.Tag() == "nem" {
			continue
		}
		p := obj.LaTexPreamble()
		if p != "" {
			out += p + "\n"
		}
	}

	out += "\\begin{document}\n"
	for _, blk := range d.Structure {
		obj := d.Objects[blk.tag]
		s := obj.LaTex(blk.ID)
		if s != "" {
			for strings.Contains(s, "[NEM:") {
				part := strings.Split(s, "[NEM:")[1]
				inlinesid := strings.Split(part, "]")[0]
				inlineid, err := strconv.ParseUint(inlinesid, 10, 32)
				if err != nil {
					return err
				}
				toreplace := fmt.Sprintf("[NEM:%d]", inlineid)
				replacewith := d.all[ID(inlineid)].LaTex(ID(inlineid))
				s = strings.Replace(s, toreplace, replacewith, 1)
			}
			out += s + "\n\n"
		}
	}
	out += "\\end{document}\n"
	outfn := strings.Replace(d.fn, ".nem", ".tex", 1)
	err := ioutil.WriteFile(outfn, []byte(out), 0644)
	if err != nil {
		return err
	}
	if pdf {
		cwd, err := os.Getwd()
		if err != nil {
			return err
		}
		_, outtex := filepath.Split(outfn)
		outpdf := filepath.Join(cwd, strings.Replace(outfn, ".tex", ".pdf", 1))
		d, err := ioutil.TempDir("", "")
		if err != nil {
			return err
		}
		defer os.RemoveAll(d)
		defer os.Chdir(cwd)
		fmt.Printf("Building pdf in %s\n", d)
		if err := os.Link(outfn, filepath.Join(d, outtex)); err != nil {
			return err
		}
		if err := os.Chdir(d); err != nil {
			os.Chdir(cwd)
			return err
		}
		pdfname := strings.Replace(outtex, ".tex", ".pdf", 1)
		buildcmd := exec.Command("pdflatex", outtex)
		if err := buildcmd.Run(); err != nil {
			return fmt.Errorf("pdflatex 0: %v", err)
		}
		buildcmd = exec.Command("pdflatex", outtex)
		if err := buildcmd.Run(); err != nil {
			return fmt.Errorf("pdflatex 1: %v", err)
		}
		mvcmd := exec.Command("mv", pdfname, outpdf)
		if err := mvcmd.Run(); err != nil {
			return fmt.Errorf("mv: %v", err)
		}
	}
	return nil
}

func (d *Document) HTML() error {
	if d.fn == "" {
		return fmt.Errorf("Cannot build LaTex document: input not parsed")
	}
	out := "<!DOCTYPE html>\n<html>\n<head>\n"
	for _, obj := range d.ordered {
		h := obj.HTMLHeader()
		if h != "" {
			out += h + "\n"
		}
	}

	out += "</head>\n<body>"
	for _, blk := range d.Structure {
		obj := d.Objects[blk.tag]
		s := obj.HTML(blk.ID)
		if s != "" {
			for strings.Contains(s, "[NEM:") {
				part := strings.Split(s, "[NEM:")[1]
				inlinesid := strings.Split(part, "]")[0]
				inlineid, err := strconv.ParseUint(inlinesid, 10, 32)
				if err != nil {
					return err
				}
				toreplace := fmt.Sprintf("[NEM:%d]", inlineid)
				replacewith := d.all[ID(inlineid)].HTML(ID(inlineid))
				s = strings.Replace(s, toreplace, replacewith, 1)
			}
			out += s + "\n\n"
		}
	}
	out += "</body>\n</html>"
	outfn := strings.Replace(d.fn, ".nem", ".html", 1)
	err := ioutil.WriteFile(outfn, []byte(out), 0644)
	return err
}
